import theMovieDb from 'themoviedb-javascript-library';
import Service from './service';

describe('Service', () => {
  const apiKey = 'testApiKey';
  const service = new Service(apiKey);

  describe('Configuration', () => {
    it('should set api key correctly', () => {
      expect(service.apiKey).toEqual(apiKey);
      expect(theMovieDb.common.api_key).toEqual(apiKey);
    });
  });

  describe('Search movies', () => {
    const searchQuery = 'testQuery';
    const searchPage = 3;
    const successData = {
      page: searchPage,
      results: [
        { poster_path: '/test-img.jpg', overview: 'Overview 1', release_date: '2013-01-01', id: 1, title: 'Title 1', vote_count: 100, vote_average: 5 },
        { poster_path: null, overview: 'Overview 2', release_date: '2015-01-01', id: 2, title: 'Title 2', vote_count: 5, vote_average: 1.22 },
      ],
      total_results: 300,
      total_pages: 100,
    };
    const successResponse = JSON.stringify(successData);
    const errorData = {
      status_message: 'Invalid API key: You must be granted a valid key.',
      success: false,
      status_code: 7,
    };
    const errorResponse = JSON.stringify(errorData);

    describe('Success response', () => {
      beforeEach(() => {
        spyOn(theMovieDb.search, 'getMovie').and.callFake((query, successCb) => {
          successCb(successResponse);
        });
      });

      it('should return response as a object', (done) => {
        service.searchMovies(searchQuery, searchPage).then((data) => {
          expect(typeof (data)).toEqual('object');
          done();
        });
      });

      it('should return correct page', (done) => {
        service.searchMovies(searchQuery, searchPage).then((data) => {
          expect(data.currentPage).toEqual(searchPage);
          done();
        });
      });

      it('should return correct total pages', (done) => {
        service.searchMovies(searchQuery, searchPage).then((data) => {
          expect(data.totalPages).toEqual(successData.total_pages);
          done();
        });
      });

      it('should return this same number of results', (done) => {
        service.searchMovies(searchQuery, searchPage).then((data) => {
          expect(data.items.length).toEqual(successData.results.length);
          done();
        });
      });
    });

    describe('Error response', () => {
      it('should return response as a object', (done) => {
        spyOn(theMovieDb.search, 'getMovie').and.callFake((query, successCb, errorCb) => {
          errorCb(errorResponse);
        });

        service.searchMovies(searchQuery, searchPage).then(() => {}, (data) => {
          expect(typeof (data)).toEqual('object');
          done();
        });
      });

      it('should return empty object when no response', (done) => {
        spyOn(theMovieDb.search, 'getMovie').and.callFake((query, successCb, errorCb) => {
          errorCb();
        });

        service.searchMovies(searchQuery, searchPage).then(() => {}, (data) => {
          expect(typeof (data)).toEqual('object');
          done();
        });
      });
    });
  });
});
