let lastTime = 0;

/**
 * Query selector
 * @param {string} selector - CSS selector
 * @returns {Object} NodeList object
 */
function $qs(selector) {
  return document.querySelector(selector);
}

/**
 * Query selector all
 * @param {string} selector - CSS selector
 * @returns {Object} NodeList object
 */
function $qsa(selector) {
  return document.querySelectorAll(selector);
}

/**
 * Register event listener
 * @param {Object} target - element object
 * @param {string} type - event type
 * @param {Function} callback - listener
 * @param {Boolean|Object} options - listener options
 */
function $on(target, type, callback, options) {
  target.addEventListener(type, callback, options);
}

/**
 * Create deferred object
 */
function $deferred() {
  this.promise = new Promise(((resolve, reject) => {
    this.resolve = resolve;
    this.reject = reject;
  }));

  this.then = this.promise.then.bind(this.promise);
  this.catch = this.promise.catch.bind(this.promise);
}

/**
 * Debounces given function execution (delays)
 * @param {Function} fn - function to debounce
 * @param {Numberic} [delay=250] - delay duration
 * @returns {Function} debounced function
 */
function $debounce(fn, delay) {
  let timer = null;

  return (...args) => {
    const that = this;

    window.clearTimeout(timer);

    timer = window.setTimeout(() => {
      fn.apply(that, args);
    }, delay || 250);
  };
}

/**
 * Will run function on RequestAnimationFrame
 * @param {Function} callback - callback to run on RAF
 */
function $onRAF(callback) {
  if (!window.requestAnimationFrame) {
    const currTime = new Date().getTime();
    const timeToCall = Math.max(0, 16 - currTime - lastTime);

    window.setTimeout(callback, timeToCall);
    lastTime = currTime + timeToCall;
  } else {
    window.requestAnimationFrame(callback);
  }
}

export { $qs, $qsa, $on, $deferred, $debounce, $onRAF };
