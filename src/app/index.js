import 'babel-polyfill';
import '../styles/index.scss';

import { $on } from './utils';
import config from './config';
import View from './view';
import Service from './service';
import Controller from './controller';

if (module.hot) {
  module.hot.accept();
}

$on(window, 'load', () => {
  const view = new View();
  const service = new Service(config.apiKey);
  const ctrl = new Controller();

  ctrl.init(view, service);
});
