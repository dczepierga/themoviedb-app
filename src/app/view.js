import { $qs, $on, $debounce, $onRAF } from './utils';
import config from './config';

/**
 * Abstract DOM view
 */
class View {
  /**
   * View initializer
   */
  constructor() {
    this.movieImgUrl = config.imgServer;

    this.$searchInput = $qs('.search__input');

    this.$list = $qs('.list');
    this.$listContainer = $qs('.list__container');
    this.$listMore = $qs('.list__more');

    this.msgList = {};
    this.msgList.loading = {
      $sel: $qs('.list__loading'),
      shown: false,
    };
    this.msgList.noResults = {
      $sel: $qs('.list__empty'),
      shown: false,
    };
    this.msgList.error = {
      $sel: $qs('.list__error'),
      shown: false,
    };
  }

  /**
   * Clear search input
   */
  searchClear() {
    this.$searchInput.value = '';
  }

  /**
   * Get search input value
   * @param {?string} value - input value to set
   * @returns {string} input value
   */
  searchValue(value) {
    if (typeof (value) !== 'undefined') {
      this.$searchInput.value = value;
    }
    return this.$searchInput.value;
  }

  /**
   * Bind search input change handler
   * @param {Function} handler - callback function
   */
  searchBind(handler) {
    const that = this;

    $on(this.$searchInput, 'keyup', $debounce(() => {
      handler(that.searchValue());
    }));
  }

  /**
   * Clear list of movies
   */
  listClear() {
    const that = this;

    $onRAF(() => {
      that.$listContainer.innerHTML = '';
    });
  }

  /**
   * Render list of movies
   * @param {Array} items - list with movies data
   * @param {boolean} [append=false] - append to the list
   */
  listRender(items, append) {
    const that = this;
    const list = [];

    items.forEach((item) => {
      const movie = that.movieRender(item);

      if (movie) {
        list.push(movie);
      }
    });

    $onRAF(() => {
      if (append) {
        that.$listContainer.innerHTML += list.join('');
      } else {
        that.$listContainer.innerHTML = list.join('');
      }
    });
  }

  /**
   * Render movie item HTML
   * @param {Object} item - movie data
   */
  movieRender(item) {
    const imgUrl = (item.poster_path) ? `${this.movieImgUrl}${item.poster_path}` : config.imgCoverDefault;

    return `<article class="movie">
              <img src="${imgUrl}" alt="${item.title}" class="movie__cover">
              <div class="movie__body">
                <header>
                  <h2 class="movie__title">${item.title}</h2>
                  <small>${item.release_date}</small>
                </header>
                <div class="movie__desc">${item.overview}</div>
                <footer class="movie__footer">
                  <a href="${config.movieUrl}${item.id}" target="_blank" class="movie__details">Details</a>
                  <div class="movie__vote">Rate: ${item.vote_average}/10</div>
                </footer>
              </div>
            </article>`;
  }

  /**
   * Show message with given type
   * @param {string} type - message type
   * @param {?string} [display='block'] - CSS display property to set
   */
  msgShow(type, display) {
    const that = this;

    if (this.msgList[type] && !this.msgList[type].shown) {
      this.msgList[type].shown = true;

      $onRAF(() => {
        that.msgList[type].$sel.style.display = display || 'block';
      });
    }
  }

  /**
   * Hide message with given type
   * @param {string} type - message type
   */
  msgHide(type) {
    const that = this;

    if (this.msgList[type] && this.msgList[type].shown) {
      this.msgList[type].shown = false;

      $onRAF(() => {
        that.msgList[type].$sel.style.display = 'none';
      });
    }
  }

  /**
   * Hide all messages
   */
  msgHideAll() {
    const that = this;

    Object.keys(this.msgList).forEach((type) => {
      that.msgHide(type);
    });
  }

  /**
   * Show more button
   * @param {?string} [display='block'] - CSS display property to set
   */
  moreShow(display) {
    const that = this;

    $onRAF(() => {
      that.$listMore.style.display = display || 'block';
    });
  }

  /**
   * Hide more button
   */
  moreHide() {
    const that = this;

    $onRAF(() => {
      that.$listMore.style.display = 'none';
    });
  }

  /**
   * Bind more click handler
   * @param {Function} handler - callback function
   */
  moreBind(handler) {
    $on(this.$listMore, 'click', (ev) => {
      ev.preventDefault();

      handler();
    });
  }
}

export default View;
