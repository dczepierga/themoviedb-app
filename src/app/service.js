import theMovieDb from 'themoviedb-javascript-library';
import { $deferred } from './utils';

/**
 * Service to get data from theMobieDb API
 */
class Service {
  /**
   * Service initializer
   * @param {string} apiKey - api key for TheMovieDB API
   */
  constructor(apiKey) {
    this.apiKey = apiKey;
    theMovieDb.common.api_key = this.apiKey;
  }

  /**
   * Search movies with given text
   * @param {string} query - search string
   * @param {?Numeric} [page=1] - page number
   * @returns {Promise} request promise
   */
  searchMovies(query, page) {
    const deferred = new $deferred();

    this.query = query;

    theMovieDb.search.getMovie({
      query: encodeURI(query),
      page: parseInt(page || 1, 10),
    }, (response) => {
      const responseParsed = JSON.parse(response);
      const data = {};

      data.currentPage = responseParsed.page || 1;
      data.totalPages = responseParsed.total_pages || 1;
      data.items = responseParsed.results || [];

      deferred.resolve(data);
    }, (response) => {
      deferred.reject((response) ? JSON.parse(response) : {});
    });

    return deferred.promise;
  }
}

export default Service;
