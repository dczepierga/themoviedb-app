import * as utils from './utils';
import config from './config';
import View from './view';

describe('View', () => {
  const view = new View();

  beforeAll(() => {
    spyOn(utils, '$onRAF').and.callFake((cb) => {
      cb();
    });

    spyOn(utils, '$debounce').and.callFake((cb) => {
      cb();
    });

    spyOn(utils, '$on').and.callFake((target, type, callback) => {
      callback({
        preventDefault: () => {},
      });
    });
  });

  describe('Search input', () => {
    const searchValue = 'testValue';

    beforeEach(() => {
      view.$searchInput = {
        value: '',
      };
    });

    it('should set value', () => {
      view.searchValue(searchValue);
      expect(view.$searchInput.value).toEqual(searchValue);
      expect(view.searchValue()).toEqual(searchValue);
    });

    it('should clear value', () => {
      view.searchValue(searchValue);
      view.searchClear();
      expect(view.searchValue()).toEqual('');
    });

    it('should bind keyup event and call callback', (done) => {
      utils.$on.calls.reset();

      view.searchBind(() => {
        done();
      });

      expect(utils.$on.calls.count()).toEqual(1);
      expect(utils.$on.calls.argsFor(0)[1]).toEqual('keyup');
    });
  });

  describe('Movie', () => {
    it('should render movie HTML', () => {
      const movieData = { poster_path: '/test-img.jpg', overview: 'Overview 1', release_date: '2013-01-01', id: 1, title: 'Title 1', vote_count: 100, vote_average: 5 };

      expect(view.movieRender(movieData).length).toBeGreaterThan(0);
    });

    it('should render movie HTML with default cover', () => {
      const movieData = { poster_path: null, overview: 'Overview 1', release_date: '2013-01-01', id: 1, title: 'Title 1', vote_count: 100, vote_average: 5 };

      expect(view.movieRender(movieData).indexOf(config.imgCoverDefault)).toBeGreaterThan(0);
    });
  });

  describe('List', () => {
    const moviesData = [
      { poster_path: '/test-img.jpg', overview: 'Overview 1', release_date: '2013-01-01', id: 1, title: 'Title 1', vote_count: 100, vote_average: 5 },
      { poster_path: null, overview: 'Overview 2', release_date: '2015-01-01', id: 2, title: 'Title 2', vote_count: 5, vote_average: 1.22 },
    ];

    beforeEach(() => {
      view.$listContainer = {
        innerHTML: '',
      };
    });

    it('should render list with 2 movies', () => {
      view.listRender(moviesData);

      expect(view.$listContainer.innerHTML.length).toBeGreaterThan(0);
      expect((view.$listContainer.innerHTML.match(/class="movie"/g) || []).length).toEqual(2);
    });

    it('should append movies to the list (4 movies)', () => {
      view.listRender(moviesData);
      view.listRender(moviesData, true);

      expect(view.$listContainer.innerHTML.length).toBeGreaterThan(0);
      expect((view.$listContainer.innerHTML.match(/class="movie"/g) || []).length).toEqual(4);
    });

    it('should clear list of movies', () => {
      view.listRender(moviesData);
      view.listClear();

      expect(view.$listContainer.innerHTML.length).toEqual(0);
    });
  });

  describe('Messages', () => {
    beforeEach(() => {
      view.msgList.msg = {
        $sel: {
          style: {
            display: 'none',
          },
        },
        shown: false,
      };

      view.msgList.msg2 = {
        $sel: {
          style: {
            display: 'none',
          },
        },
        shown: false,
      };
    });

    it('should show msg with default display (block)', () => {
      view.msgShow('msg');

      expect(view.msgList.msg.shown).toBeTruthy();
      expect(view.msgList.msg.$sel.style.display).toEqual('block');
    });

    it('should show msg with custom display (inline-block)', () => {
      view.msgShow('msg', 'inline-block');

      expect(view.msgList.msg.shown).toBeTruthy();
      expect(view.msgList.msg.$sel.style.display).toEqual('inline-block');
    });

    it('should hide msg', () => {
      view.msgShow('msg');
      view.msgHide('msg');

      expect(view.msgList.msg.shown).toBeFalsy();
      expect(view.msgList.msg.$sel.style.display).toEqual('none');
    });

    it('should hide all msg', () => {
      view.msgShow('msg');
      view.msgShow('msg2');
      view.msgHideAll();

      expect(view.msgList.msg.shown).toBeFalsy();
      expect(view.msgList.msg.$sel.style.display).toEqual('none');
      expect(view.msgList.msg2.shown).toBeFalsy();
      expect(view.msgList.msg2.$sel.style.display).toEqual('none');
    });
  });

  describe('More button', () => {
    beforeEach(() => {
      view.$listMore = {
        style: {
          display: 'none',
        },
      };
    });

    it('should show with default display (block)', () => {
      view.moreShow();

      expect(view.$listMore.style.display).toEqual('block');
    });

    it('should show with custom display (inline-block)', () => {
      view.moreShow('inline-block');

      expect(view.$listMore.style.display).toEqual('inline-block');
    });

    it('should hide', () => {
      view.moreHide();

      expect(view.$listMore.style.display).toEqual('none');
    });

    it('should bind click event and call callback', (done) => {
      utils.$on.calls.reset();

      view.moreBind(() => {
        done();
      });

      expect(utils.$on.calls.count()).toEqual(1);
      expect(utils.$on.calls.argsFor(0)[1]).toEqual('click');
    });
  });
});
