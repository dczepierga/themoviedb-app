const config = {
  apiKey: 'b55935d2565d2d6f9200cc6d72a925d5',
  imgServer: 'http://image.tmdb.org/t/p/w185',
  imgCoverDefault: 'assets/no-cover.png',
  movieUrl: 'https://www.themoviedb.org/movie/',
};

export default config;
