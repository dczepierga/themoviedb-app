/**
 * App controller to connect service with data and view
 */
class Controller {
  /**
   * Controller initializer
   * @param  {Object} view - the view instance
   * @param  {Object} service - the service instance
   */
  init(view, service) {
    const that = this;

    this.view = view;
    this.service = service;

    this.query = '';
    this.page = 1;

    // load movies on search
    this.view.searchBind((query) => {
      this.page = 1;
      this.query = query || '';

      that.view.listClear();

      that.view.msgHideAll();
      that.view.moreHide();

      if (this.query.length > 0) {
        that.view.msgShow('loading');

        service.searchMovies(this.query).then((data) => {
          that.view.msgHide('loading');

          that.page = data.currentPage || 1;

          if (data.items.length > 0) {
            that.view.listRender(data.items);
          } else {
            that.view.msgShow('noResults');
          }

          if (data.currentPage < data.totalPages) {
            that.view.moreShow();
          } else {
            that.view.moreHide();
          }
        }, () => {
          that.view.msgHide('loading');
          that.view.msgShow('error');
        });
      }
    });

    // load more movies on btn click
    this.view.moreBind(() => {
      that.view.moreHide();
      that.view.msgShow('loading');

      service.searchMovies(this.query, that.page + 1).then((data) => {
        that.view.msgHide('loading');

        that.page = data.currentPage || 1;

        that.view.listRender(data.items, true);

        if (data.currentPage < data.totalPages) {
          that.view.moreShow();
        } else {
          that.view.moreHide();
        }
      }, () => {
        that.view.msgHide('loading');
        that.view.msgShow('error');
      });
    });
  }
}

export default Controller;
