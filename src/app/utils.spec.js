import * as utils from './utils';

describe('Utils', () => {
  describe('$qs - document query selector', () => {
    it('should call document method with selector', () => {
      spyOn(document, 'querySelector').and.callThrough();

      utils.$qs('.selector');

      expect(document.querySelector.calls.count()).toEqual(1);
      expect(document.querySelector.calls.argsFor(0)[0]).toEqual('.selector');
    });
  });

  describe('$qsa - document query selector - all elems', () => {
    it('should call document method with selector', () => {
      spyOn(document, 'querySelectorAll').and.callThrough();

      utils.$qsa('.selector');

      expect(document.querySelectorAll.calls.count()).toEqual(1);
      expect(document.querySelectorAll.calls.argsFor(0)[0]).toEqual('.selector');
    });
  });

  describe('$on - add event listener on target elem', () => {
    it('should add event listener to the target', () => {
      const func = () => (true);
      const options = { option: 'test' };

      spyOn(document, 'addEventListener').and.callThrough();

      utils.$on(document, 'click', func, options);

      expect(document.addEventListener.calls.count()).toEqual(1);
      expect(document.addEventListener.calls.argsFor(0)[0]).toEqual('click');
      expect(document.addEventListener.calls.argsFor(0)[1]).toEqual(func);
      expect(document.addEventListener.calls.argsFor(0)[2]).toEqual(options);
    });
  });

  describe('$deferred - deferred object', () => {
    let deferred;

    beforeEach(() => {
      deferred = new utils.$deferred();
    });

    it('should be object', () => {
      expect(typeof (deferred)).toBe('object');
    });

    it('promise should return instance of Promise', () => {
      expect(deferred.promise instanceof Promise).toBeTruthy();
    });

    it('then and catch should return function', () => {
      expect(typeof (deferred.then)).toBe('function');
      expect(typeof (deferred.catch)).toBe('function');
    });
  });

  describe('$debounce - debounce function execution', () => {
    it('should call callback', (done) => {
      const func = () => {
        done();
      };

      utils.$debounce(func)();
    });

    it('should call callback only once', () => {
      const spy = jasmine.createSpy('func');
      const debounce = utils.$debounce(spy, 100);

      jasmine.clock().install();
      debounce();
      debounce();
      debounce();
      jasmine.clock().tick(101);
      jasmine.clock().uninstall();

      expect(spy.calls.count()).toEqual(1);
    });
  });

  describe('$onRAF - request animation frame', () => {
    it('should call window method if available', () => {
      const func = () => (true);

      spyOn(window, 'requestAnimationFrame').and.callThrough();

      utils.$onRAF(func);

      expect(window.requestAnimationFrame.calls.count()).toEqual(1);
      expect(window.requestAnimationFrame.calls.argsFor(0)[0]).toEqual(func);
    });

    it('should use fallback if not available', (done) => {
      const func = () => {
        done();
      };

      delete window.requestAnimationFrame;
      utils.$onRAF(func);
    });
  });
});
