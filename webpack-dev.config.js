/* eslint-disable import/no-extraneous-dependencies */
const webpack = require('webpack');
const webpackConfig = require('./webpack-common.config');

const devServerPort = 8080;

webpackConfig.devtool = 'cheap-eval-source-map';

webpackConfig.entry.unshift(
  `webpack-dev-server/client?http://localhost:${devServerPort}`,
  'webpack/hot/dev-server'
);

webpackConfig.plugins.push(
  new webpack.HotModuleReplacementPlugin()
);

webpackConfig.module.loaders.push({
  test: /\.scss$/i,
  loaders: ['style', 'css', 'sass'],
}, {
  test: /\.js$/,
  loader: 'eslint',
  exclude: /node_modules/,
});

webpackConfig.devServer = {
  contentBase: './src',
  hot: true,
  port: devServerPort,
  inline: true,
  progress: true,
};

module.exports = webpackConfig;
