const webpackConfig = require('./webpack-dev.config');

process.env.NODE_ENV = 'test';

module.exports = (config) => {
  config.set({
    files: [
      'src/app/**/*.spec.js',
    ],
    browsers: ['Chrome'],
    frameworks: ['jasmine'],
    singleRun: true,
    preprocessors: {
      'src/app/**/*.spec.js': ['babel'],
      'src/app/**/*.js': ['webpack'],
    },
    webpack: webpackConfig,
    webpackServer: {
      noInfo: true,
    },
    reporters: ['spec', 'coverage'],
    coverageReporter: {
      reporters: [
        { type: 'text' },
        { type: 'html', dir: 'coverage/' },
      ],
    },
  });
};
