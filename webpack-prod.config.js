/* eslint-disable import/no-extraneous-dependencies */
const webpack = require('webpack');
const webpackConfig = require('./webpack-common.config');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

const extractSASS = new ExtractTextPlugin('style.css');

webpackConfig.plugins.push(
  new CleanWebpackPlugin(['dist']),
  new CopyWebpackPlugin([{ from: 'src/assets', to: 'assets' }]),
  new webpack.optimize.OccurenceOrderPlugin(),
  new webpack.optimize.UglifyJsPlugin({
    compressor: {
      warnings: false,
    },
  }),
  extractSASS
);

webpackConfig.module.loaders.push({
  test: /\.scss$/i,
  loader: extractSASS.extract(['css', 'sass']),
});

module.exports = webpackConfig;
