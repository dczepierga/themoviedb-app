# The Movie DB App
Movie quick search app created to fetch data using The Movie DB API and present results in the simple form.

All results are presented after user starts typing in the search form with option to load more results.
If user need more details about movie, then can use "Details" button which redirect user to the movie page on The Movie DB website.

# Overview
Application created in clean JavaScript ES2015 with Babel Polyfill for backward compatibility.

## Requirements
* NodeJS 6.9.0 or above
* NPM 3.0.0 or above

## Installation
`npm install` to install all dependencies

After install all dependencies following commands are available:

* `npm run prod` - prepare dist version of the app for the production (`dist` directory)
* `npm run dev` - prepare dev version of the app with webserver and HRM (http://localhost:8080/)
* `npm run docs` - prepare documentation from the code (`docs` directory)
* `npm run lint` - lint all JavaScript and Sass files
* `npm run lint:js` - lint all JavaScript files
* `npm run lint:sass` - lint all Sass files
* `npm run test` - run unit tests and save code coverage report (`coverage` directory)

## Dependencies
* Babel Polyfill (https://babeljs.io/docs/usage/polyfill/)
* Normalize - SCSS version (https://github.com/JohnAlbin/normalize-scss)
* The Movie DB JavaScript library (https://github.com/cavestri/themoviedb-javascript-library)

## Dev dependencies
* Webpack (https://webpack.github.io/)
* Babel (https://babeljs.io/)
* Node Sass (https://github.com/sass/node-sass)
* ESLint (http://eslint.org/)
* Sass Lint (https://github.com/sasstools/sass-lint)
* JSDoc (https://github.com/jsdoc3/jsdoc)
* Jasmine (http://jasmine.github.io/)
* Karma (https://karma-runner.github.io/)

## Coding standards
* JavaScript - Airbnb Base (https://github.com/airbnb/javascript)
* Sass - Airbnb (https://github.com/airbnb/css) - converted to the Sass Lint config

## Unit tests - code coverage
* Statements: 96.23% (102/106)
* Branches: 82.35% (28/34)
* Functions: 100% (23/23)
* Lines: 98.95% (94/95)
